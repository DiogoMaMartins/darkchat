'use strict';

const nmap = require('node-nmap');
nmap.nmapLocation = "nmap";

const Telegram = require('telegram-node-bot');

class NmapController extends Telegram.TelegramBaseController {
    nmapHandler($) {
        try {
            let target = $.message.text.split(' ').slice(1).join(' ');
            console.log('im here', target)

            let osAndPortScan = new nmap.OsAndPortScan(`${target}`);

            osAndPortScan.on('complete', function(data) {

                console.log(JSON.stringify(data));
                   $.sendMessage(JSON.stringify(data));

            })

            osAndPortScan.on('error', function(error) {
                console.log(error);
            })

            osAndPortScan.startScan();

         
        } catch (err) {
            console.log("err", err)
              $.sendMessage(JSON.stringify(err));
        }




    }

    get routes() {
        return {
            'nmapCommand': 'nmapHandler'
        }
    }
}

module.exports = NmapController;