'use strict';

const nmap = require('node-nmap');
const Traceroute = require('nodejs-traceroute');
const tracer = new Traceroute();

nmap.nmapLocation = "nmap";

const Telegram = require('telegram-node-bot');

class TraceRouteController extends Telegram.TelegramBaseController {
    tracerouteHandler($) {
        try {
            let target = $.message.text.split(' ').slice(1).join(' ');

            tracer
                .on('pid', (pid) => {
                	 $.sendMessage(JSON.stringify(`pid: ${pid}`));
                    console.log(`pid: ${pid}`);
                })
                .on('destination', (destination) => {
                    $.sendMessage(JSON.stringify(destination));
                })
                .on('hop', (hop) => {
                	  $.sendMessage(JSON.stringify(hop));
                })
                .on('close', (close) => {
                	 $.sendMessage(JSON.stringify(close));
                });

                tracer.trace(`${target}`);


        } catch (err) {
            $.sendMessage(JSON.stringify(err));
        }
    }

        get routes() {
            return {
                'tracerouteCommand': 'tracerouteHandler'
            }
        }
    }

module.exports = TraceRouteController;