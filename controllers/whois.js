'use strict';

const whois = require('whois');


const Telegram = require('telegram-node-bot');

class WhoisController extends Telegram.TelegramBaseController {
    whoisHandler($) {
        try {
            let target = $.message.text.split(' ').slice(1).join(' ');

            whois.lookup(`${target}`, function(err, data) {
                $.sendMessage(JSON.stringify(data));
            })

        } catch (err) {
            console.log("err", err)
            $.sendMessage(JSON.stringify(err));
        }




    }

    get routes() {
        return {
            'whoisCommand': 'whoisHandler'
        }
    }
}

module.exports = WhoisController;