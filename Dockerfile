From node:8.6-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json ./
Run npm install

COPY . .

EXPOSE 7777
CMD ["npm", "start"]
