'use strict'

const Telegram = require('telegram-node-bot'),
    tg = new Telegram.Telegram('739033324:AAGrjGFN5JQqRLIl_3JJax-K3vDw3m9HzLU', {
        workers: 1
    });

const NmapController = require('./controllers/nmap'),
    OtherwiseController = require('./controllers/otherwise');

const TraceRouteController = require('./controllers/traceroute');
const traceroute = new TraceRouteController();

const WhoisController = require('./controllers/whois');
const whois = new WhoisController();

tg.router.when(new Telegram.TextCommand('/nmap', 'nmapCommand'), new NmapController())
    .when(new Telegram.TextCommand('/traceroute', 'tracerouteCommand'), traceroute)
    .when(new Telegram.TextCommand('/whois', 'whoisCommand'), whois)

    .otherwise(new OtherwiseController())